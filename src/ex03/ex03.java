package ex03;

import examples.Passwords;

import java.util.Arrays;

/**
 * The following is a secure hash (SHA-512, 1 byte salt, 5000 iterations):
 * vIhaK/N/w9ny3vwnE766DI2BbdmnRK8Oc5gx7zOsszQP+NOBbX0avHpJj+nWdv22J8iJtZQKBdbWFd+S30aYMw==
 * Using the hash() and isExpectedPassword() functions in the Passwords class provided, determine what the password is. In addition, indicate how long it took to break the password.
 * Hint : The password is no longer than 3 characters, and uses lowercase alpha and numeric characters (a-z, 0-9).
 **/

public class ex03 { // brute force approach
    public static void main(String[] args) {
        int max_password_length = 3; // assumption of password length
        String[] charset = "abcdefghijklmnopqrstuvwxyz0123456789".split(""); // assumption that password contains only letters
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);
        int count = 0;
        long startTime = System.currentTimeMillis();

        while (true) { // iterator - generate every possible password containing the letters in given string array up to given max length (from 1 to 4)
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }
                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }

            // new salt array
            byte[] salt = new byte[1];

            // Do password checking stuff here, break when you are done

            // check that salted hash matches password
            // one for loop for 1 byte salt // nest another loop for 2 byte salt
            for (int i = 0; i < 256; i++) {
                salt[0] = (byte) i;
                if (Passwords.isExpectedPassword(
                        password.toCharArray(),
                        salt,
                        5000,
                        Passwords.base64Decode("vIhaK/N/w9ny3vwnE766DI2BbdmnRK8Oc5gx7zOsszQP+NOBbX0avHpJj+nWdv22J8iJtZQKBdbWFd+S30aYMw=="))) {
                    // If true
                    System.out.println(password + " is correct");
                    long duration = System.currentTimeMillis() - startTime;
                    System.out.println("It took " + duration + " ms!");
                    return;
                }
            }
            if (count++ % 100 == 0) {
                System.out.println(password);
            }
        }
    }
}