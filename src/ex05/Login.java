package ex05;

import examples.Passwords;

import javax.servlet.http.HttpServlet;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.*;

/**
 * Created by ylin183 on 23/05/2017.
 */
public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("Start of doPost");

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n<head><meta charset=\"UTF-8\"><title>Server response</title>");

        out.println("<!-- Latest compiled and minified CSS -->\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");

        out.println("</head>\n<body>");

        out.println("<div class=\"container\">");

        out.println("<h3>Login</h3>");

        // Load DB driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Loaded driver");
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<p>Database driver can't be loaded!</p></body></html>");
            return;
        }

        // Set the database name to your database
        String url = "jdbc:mysql://mysql1.sporadic.co.nz:3306/";
        String dbName = "ylin183";

        try {
            // Get and store variables from login form
            String username = request.getParameter("username");
            String password = request.getParameter("password");

            System.out.println("username: " + username + ", password: " + password);

            // Connect to database
            try (Connection conn = DriverManager.getConnection(url + dbName, Config.getProperties())) {

                // TODO get the passIterations and salt from DB

                // 1. try to grab both at the same time, store in resultset
                int iterationsDB = 0;
                byte[] saltDB = null;
                byte[] passDB = null;

                try (PreparedStatement pst = conn.prepareStatement("SELECT * FROM web_lab_19_userDB WHERE username = ?")) {
                    pst.setString(1, username);
                    ResultSet rs = pst.executeQuery();
                    if (rs.next()) {
                        passDB = Passwords.base64Decode(rs.getString(4));
                        iterationsDB = rs.getInt(5);
                        saltDB = Passwords.base64Decode(rs.getString(6));

                        // 2. Convert the plain text password from user login to hash & salted temp pass
                        char[] pass = password.toCharArray();
//                        byte[] hashed = Passwords.hash(pass,saltDB,iterationsDB);
//                        String passwordHashed = Passwords.base64Encode(hashed);
//                        System.out.println("Hashed pass: " + passwordHashed);

                        // 3. Compare password from DB with password from login form
                        if (Passwords.isExpectedPassword(pass, saltDB, iterationsDB, passDB)) {
                            out.println("<p>Successfully logged in.</p>");
                        } else {
                            out.println("<p>Username or password is incorrect or does not exist.</p>");
                        }
                    } else {
                        out.println("<p>Username or password is incorrect or does not exist.</p>");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<p>" + e.getMessage() + "</p>");
        }
        out.println("</div></body></html>");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ServletException("Use POST instead please :)");
    }

}
