package ex05;

/**
 * Created by ylin183 on 23/05/2017.
 */

import examples.Passwords;

//import java.nio.charset.Charset;
//import java.nio.charset.StandardCharsets;
//import java.util.Arrays;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Map.Entry;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.*;

public class Register extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ServletException("Use POST instead please :)");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("Start of doPost");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n<head><meta charset=\"UTF-8\"><title>Server response</title>");

        out.println("<!-- Latest compiled and minified CSS -->\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");

        out.println("</head>\n<body>");

        out.println("<div class=\"container\">");

        // Load DB driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Loaded driver");
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<p>Database driver can't be loaded!</p></body></html>");
            return;
        }

        // Set the database name to your database
        String url = "jdbc:mysql://mysql1.sporadic.co.nz:3306/";
        String dbName = "ylin183";

        try {
            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            System.out.println("username: " + username + ", email: " + email + ", password: " + password);

            // Generate random password hash iteration
            int iterations = (int)(Math.random()*(255));
            String passIterations = Integer.toString(iterations);
            System.out.println("Iterations: " + passIterations);

            // Generate random salt
            byte[] salt = Passwords.getNextSalt();
            String passTheSalt = Passwords.base64Encode(salt);
            System.out.println("Random salt: " + passTheSalt);

            // Generate hash for password (use salt and iteration from above), store into new variable passwordHash
            char[] pass = password.toCharArray();
            byte[] hashed = Passwords.hash(pass,salt,iterations);
            String passwordHashed = Passwords.base64Encode(hashed);
            System.out.println("Hashed pass: " + passwordHashed);

            // Create connection to server and write preparedStatement to SQL
            try (Connection conn = DriverManager.getConnection(url + dbName, Config.getProperties())) {
                try (PreparedStatement pst = conn.prepareStatement("INSERT INTO web_lab_19_userDB (username,email,password,passIterations,passTheSalt) VALUES(?,?,?,?,?)")) {

                    pst.setString(1, username);
                    pst.setString(2, email);
                    pst.setString(3, passwordHashed);
                    pst.setString(4, passIterations);
                    pst.setString(5, passTheSalt);

                    System.out.println("Set PreparedStatement params");

                    int i = pst.executeUpdate();

                    System.out.println("Executed update");

                    String msg;
                    if (i != 0) {
                        msg = "Registration successful!";
                        out.println("<h3>" + msg + "</h3>");
                    } else {
                        msg = "Registration failed. Please try again!";
                        out.println("<h3>" + msg + "</h3>");
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<p>" + e.getMessage() + "</p>");
        }

        out.println("<div></body></html>");

    }


}
