package ex05;

import java.util.Properties;

/**
 * Allows the program to get properties such as username & password from a centralized location.
 */
public class Config {

    public static final String DB_NAME = "ylin183";

    private static Properties properties;

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties(System.getProperties());
            properties.setProperty("user", "ylin183");
            properties.setProperty("password", "381nily");
            properties.setProperty("useSSL", "true");
        }
        return properties;
    }

}
