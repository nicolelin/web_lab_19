package ex05;

/**
 * Created by ylin183 on 23/05/2017.
 */

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class DisplayHTML extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */

    public DisplayHTML() {
        super();
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        //displayBasic(request, response);

        displayHTML(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }


    private void displayBasic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques

        // https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html

        Map<String, String[]> map = request.getParameterMap();
        Iterator<Entry<String, String[]>> i = map.entrySet().iterator();
        response.getWriter().println("\n\nkey: values");

        while (i.hasNext()) {
            Entry<String, String[]> entry = i.next();
            String key = entry.getKey();
            String[] values = entry.getValue();
            response.getWriter().print("\n" + key.toUpperCase() + ": ");
            for (String value : values) {
                response.getWriter().print(value + ",");
            }
        }
        response.getWriter().print("\n");
    }


    private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques

        // https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html

        // "/" is relative to the context root (your web-app name)
        // don't add your web-app name to the path

        //RequestDispatcher view = request.getRequestDispatcher("/response.html"); // works, but /response.jsp causes 500 "jsp support not configured"
        //view.forward(request, response);


        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n<head><meta charset=\"UTF-8\"><title>Server response</title>");

        out.println("<!-- Latest compiled and minified CSS -->\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");

//        out.println("    <!-- MDBootstrap on CDNJS -->\n" +
//                "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css\" />");

        out.println("<!-- Flat UI Bootstrap on CDNJS -->\n" +
                "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.3.0/css/flat-ui.min.css\">");

        out.println("</head>\n<body>");

        out.println("<div class=\"container\">");

        out.println("<h1>Server side response</h1>");
        out.println("<p>Thanks for your submission. The values sent to the server are as follows:</p>");

        out.println("<div class=\"table-responsive\">");

        out.println("<table class=\"table table-hover\">");

        out.println("<thead><tr><td><b>Questions</b></td><td><b>Answers</b></td></tr></thead>");

        Map<String, String[]> map = request.getParameterMap();
        Iterator<Entry<String, String[]>> i = map.entrySet().iterator();

        out.println("<tbody");

        while (i.hasNext()) {

            out.println("<tr>");

            Entry<String, String[]> entry = i.next();
            String key = entry.getKey(); //.toUpperCase();
            String[] values = entry.getValue();

            if (key.contains("submit") || key.contains("button")) {
                continue;
            }

            out.print("<td>");

            int index = key.indexOf("[]");
            if (index != -1) {
                key = key.substring(0, index);
            }

            out.print("<b>" + key + "</b>" + ": ");

            out.print("</td>");

            out.print("<td>");

            for (String value : values) {

                out.print(value);
                out.print("<br>");
            }
            out.print("</td>");


            out.println("</tr>");
        }

        out.println("</tbody");

        out.println("</table>");

        out.println("</div>");

        out.println("</div>");

        out.println("</body></html>");
    }

}
