package ex01;

import examples.Passwords;

import java.util.Arrays;

/**
 * The following is an insecure hash (SHA-512, no salt, no iteration):
 * wJmmp8BE3aqU8HpOQ8vVmtE9P0r6ZXh3uiHP+ZQx3CU9e6pcjD14ay7j+ljBs+gGicEcKS+pKdHn6VUpDGFgnQ==
 * Using the insecureHash() and isInsecureHashMatch() functions in the Passwords class provided, determine what the password is. In addition, indicate how long it took to break the password.
 * Hint : The password is no longer than 5 characters, and uses lowercase alpha and numeric characters (a-z, 0-9).
 **/

public class ex01 { // brute force approach
    public static void main(String[] args) {
        int max_password_length = 5; // assumption of password length
        String[] charset = "abcdefghijklmnopqrstuvwxyz0123456789".split(""); // assumption that password contains only letters
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);

        while (true) { // iterator - generate every possible password containing the letters in given string array up to given max length (from 1 to 5) // don't change
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }

                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }

            // Do password checking stuff here, break when you are done

            // check that hash matches password
            if (Passwords.isInsecureHashMatch(password, Passwords.base64Decode("wJmmp8BE3aqU8HpOQ8vVmtE9P0r6ZXh3uiHP+ZQx3CU9e6pcjD14ay7j+ljBs+gGicEcKS+pKdHn6VUpDGFgnQ=="))) {
                // If true
                System.out.println(password + " is correct");
                break;
            }
//            System.out.println(password);
        }
    }
}