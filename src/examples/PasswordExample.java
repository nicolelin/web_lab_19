package examples;

import java.util.Arrays;

public class PasswordExample { // brute force approach
    public static void main(String[] args) {
        int max_password_length = 5; // assumption of password length
        String[] charset = "abcdefghijklmnopqrstuvwxyz".split(""); // assumption that password contains only letters
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);

        while (true) { // generate every possible password containing the letters in given string array up to given max length
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }

                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }

            // Do password checking stuff here, break when you are done
			
			// check that hash matches password
			
            System.out.println(password);
        }
    }
}