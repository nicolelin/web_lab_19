/**
Develop a table for storing secure login information. Your table should include a username field, and any necessary columns for storing secure password information.
Note : The column types and sizes you use for storing the password information will depend on if you are storing BLOB data (plain old byte array information) or base64 encoded string representations of the hash and salt.
**/

DROP TABLE IF EXISTS web_lab_19_userDB;

CREATE TABLE web_lab_19_userDB
(
  userID INT AUTO_INCREMENT UNIQUE NOT NULL,
  username VARCHAR(20) UNIQUE NOT NULL,
  email VARCHAR(256) NOT NULL,
  password VARCHAR(256) NOT NULL,
  passIterations INT NOT NULL,
  passTheSalt VARCHAR(256) NOT NULL,
  PRIMARY KEY (userID)
);