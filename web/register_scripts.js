"use strict";

$(document).ready(function () {

    var password = document.getElementById("password"),
        password_confirm = document.getElementById("password_confirm");

    function validatePassword() {
        if (password.value != password_confirm.value) {
            password_confirm.setCustomValidity("Passwords don't match.");
        } else {
            password_confirm.setCustomValidity("");
        }
    }

    password.onchange = validatePassword;
    password_confirm.onkeyup = validatePassword;

});